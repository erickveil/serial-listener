/**
 * leonardoseriallistener.h
 * Erick Veil
 * Copyright 2021 Erick Veil
 */
#ifndef LEONARDOSERIALLISTENER_H
#define LEONARDOSERIALLISTENER_H

#include "functional"

#include <QByteArray>
#include <QList>
#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QString>

#include "staticlogger.h"

/**
 * @brief The LeonardoSerialListener class
 * This class is intended to act as a serial listener for the Arduino Leonardo.
 *
 * It will log out whatever is recieved and is non-blocking.
 *
 * A callback can be provided wich will execute with whatever the listener
 * recieved.
 * Use a lambda for the callback. See
 * https://erickveil.github.io/c++11,/lambda,/functional,/programming,/closures/2016/10/19/C++-Lambdas.html
 *
 * This listener does not reply to the client, and opens the serial port
 * as read only.
 *
 * If the parse callback is not set, it will just log whatever you send.
 *
 * Never closes until the program ends.
 *
 * Automatically finds which serial port the Leonardo is on. Takes the first
 * one if there are several. Takes the first serial port if it can't fine
 * a Leonardo. Gives an error if it can't find a serial device.
 */
class LeonardoSerialListener : public QObject
{
    QSerialPort _connection;
    QSerialPortInfo _serialInfo;

    bool _isInit = false;

    std::function<void (QByteArray)> _parseCallback;

    Q_OBJECT
public:
    /**
     * @brief LeonardoSerialListener
     * @param parent
     */
    explicit LeonardoSerialListener(QObject *parent = nullptr);

    /**
     * @brief init
     * This method must be called before start().
     */
    void init();

    /**
     * @brief init
     * Can be called in place of overloaded init().
     * (Calls init after setting values).
     * @param parseCb Pass a lambda to call when new data is recieved.
     */
    void init(std::function<void (QByteArray)> parseCb);

    /**
     * @brief start
     * Starts the listener.
     * init() or one of its overloads must be called before this, otherwise
     * you will just get an error message in the log.
     */
    void start();

    /**
     * @brief logSerialInfo
     * Static helper function to see what's going on with all of your serial
     * ports.
     */
    static void logSerialInfo();

    /**
     * @brief getName
     * @return gets a human readable name for this that gets sent to the log.
     */
    QString getName();

private slots:
    void _eventAboutToClose();
    void _eventBytesWritten(qint64 bytes);
    void _eventChannelBytesWritten(int channel, qint64 bytes);
    void _eventChannelReadyRead(int channel);
    void _eventReadChannelFinished();
    void _eventReadyRead();

private:
    QString _getArduinoPort();
};

#endif // LEONARDOSERIALLISTENER_H
