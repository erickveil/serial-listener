/**
 * serialrelay.h
 * Erick Veil
 * 2019-07-25
 * Copyright 2019 ComTech Communications
 */
#ifndef SERIALRELAY_H
#define SERIALRELAY_H

#include <QByteArray>
#include <QDebug>
#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QString>
#include <QTimer>

#include "staticlogger.h"

namespace stnctl {

/**
 * @brief The SerialRelay class
 * This class represents a physical relay from the Numato Lab N Channel USB
 * Relay Module.
 *
 * The target program requires the relay to be the granular unit, so I did
 * not encapsulate the entire module in a single class.
 *
 * The interface for this class allow simple activation and deactivation of a
 * single relay with a method call without worrying about serial ports or
 * commands.
 *
 * This class was purposely not given a copy constructor or assignment
 * opperator, as this class should be dynamically instantiated and passes by
 * reference. I'm too lazy to look up the proper pattern for forcing that
 * useage, so user beware. I did this because I'm also too lazy to create an
 * interface for this class.
 *
 * If, in the future, we need to use a different brand USB module that uses
 * a different protocol, you can use this class as the interface, and just
 * extend it with the new class (as long as it has only been used as described
 * above).
 */
class SerialRelay : public QObject
{
    Q_OBJECT

    int _switchNumber;
    QString _portName;
    int _activationTimeMs;

    bool _isInit = false;

    QSerialPort _serialPort;
    QSerialPortInfo _portInfo;

    QByteArray _writeData;
    qint64 _bytesWritten = 0;
    QTimer _writeTimer;
    QTimer _dispatchTimer;

    int _busyRetries = 0;

    bool _isNotConfigured = false;

    /**
     * @brief EXECUTE_CMD
     * The Numato protocol requires a carriage return at the end of every
     * command string to trigger it into parsing the data.
     */
    const char EXECUTE_CMD = 13;

    /**
     * @brief BUSY_WAIT_MS
     * If the module is busy when we try to send a command, we wait this long
     * in milliseconds to try again.
     */
    const int BUSY_WAIT_MS = 5;

    /**
     * @brief BUSY_RETRIES Only retry for 1 second if port is busy before
     * quitting and reporting an error.
     */
    const int MAX_BUSY_RETRIES = 1000 / BUSY_WAIT_MS;

public:

    /**
     * @brief getName
     * @return
     */
    QString getName();

    /**
     * @brief SerialRelay
     * Creates a null relay object. The init method must be called before it
     * can be used.
     * @param parent
     */
    explicit SerialRelay(QObject *parent = nullptr);

    /**
     * @brief SerialRelay
     * @param switchNumber The numeric ID of the relay switch, from 0 to n,
     * where n is the number of switches on the module. If you provide an
     * invalid number, nothing will happen.
     * @param activationTimeMs The amount of time to wait after activation
     * before a deactivation time is sent. Each relay gets its own activation
     * timer.
     * @param portName The name of the serial port in Linux. The default is the
     * typical name for the first module inserted into the machine.
     */
    SerialRelay(int switchNumber, int activationTimeMs,
                QString portName = "ttyACM0");

    /**
     * @brief init
     * @param switchNumber The numeric ID of the relay switch, from 0 to n,
     * where n is the number of switches on the module. If you provide an
     * invalid number, nothing will happen.
     * @param activationTimeMs The amount of time to wait after activation
     * before a deactivation time is sent. Each relay gets its own activation
     * timer.
     * @param portName The name of the serial port in Linux. The default is the
     * typical name for the first module inserted into the machine.
     */
    void init(int switchNumber, int activationTimeMs,
              QString portName = "ttyACM0");

    /**
     * @brief activateRelay
     * Turns this relay on, then starts a timer that will eventually shut it
     * off again.
     */
    void activateRelay();

    /**
     * @brief activatePersistentRelay
     * Turns this relay on.
     * Does NOT start a timer to turn it off agian.
     */
    void activatePersistentRelay();

    /**
     * @brief deactivateRelay
     * Manually turns the relay off.
     */
    void deactivateRelay();

    /**
     * @brief isNull
     * @return True if ini hasn't been called yet.
     */
    bool isNull();

    /**
     * @brief getPortInfo
     * @return Serial Port info as a string
     * eg:
     * Name:  "ttyACM0"
     * Location:  "/dev/ttyACM0"
     * Description:  "Numato Lab 8 Channel USB Relay Module"
     * Mfg:  "Numato Systems Pvt. Ltd."
     * Serial:  ""
     * Vendor ID:  10777
     * Product ID:  3074
     * Busy:  false
     * Valid:  true
     */
    QString getPortInfoStr();

    /**
     * @brief getShortPortInfoStr
     * @return Shorter version of the port info string, used in log ID
     * eg:
     * "ttyACM0 (Numato Lab 8 Channel USB Relay Module) Valid: true Busy: false"
     */
    QString getShortPortInfoStr();

    /**
     * @brief getPortInfoObj
     * @return The port info object.
     */
    QSerialPortInfo getPortInfoObj();

private:
    QByteArray _createActivateCommand();
    QByteArray _createDeactivateCommand();
    void _write(QByteArray cmd);
    QString _hexdump(QByteArray bytes);

signals:

private slots:
    void _eventBytesWritten(qint64 bytes);
    void _eventSerialPortTimeout();
    void _eventWriteError(QSerialPort::SerialPortError error);
    void _eventRetryWrite();
    void _eventDispatchTimerTimeout();
    void _eventDataTerminalReadyChanged(bool set);
};

} // namespace stnctl

#endif // SERIALRELAY_H
