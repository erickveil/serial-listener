#include "serialrelay.h"

namespace stnctl {

QString SerialRelay::getName()
{
    return  getShortPortInfoStr() + " - switch "
            + QString::number(_switchNumber);
}

SerialRelay::SerialRelay(QObject *parent) : QObject(parent)
{

}

SerialRelay::SerialRelay(int switchNumber, int activationTimeMs,
                         QString portName)
{
    init(switchNumber, activationTimeMs, portName);
}

void SerialRelay::init(int switchNumber, int activationTimeMs, QString portName)
{
    if (_isInit) { return; }
    _isInit = true;
    _switchNumber = switchNumber;
    _activationTimeMs = activationTimeMs;
    _portName = portName;
    if (portName == "") { _isNotConfigured = true; }

    _serialPort.setPortName(portName);
    _serialPort.setBaudRate(QSerialPort::Baud9600);

    _portInfo = QSerialPortInfo(portName);

    _writeTimer.setSingleShot(true);
    _writeTimer.setInterval(5000);

    connect(&_serialPort, SIGNAL( bytesWritten(qint64) ),
            this, SLOT( _eventBytesWritten(qint64) )
            );
    connect(&_serialPort, SIGNAL( errorOccurred(QSerialPort::SerialPortError) ),
            this, SLOT( _eventWriteError( QSerialPort::SerialPortError) )
            );
    connect(&_serialPort, SIGNAL( dataTerminalReadyChanged(bool) ),
            this, SLOT( _eventDataTerminalReadyChanged(bool) )
            );


    connect(&_writeTimer, SIGNAL( timeout() ),
            this, SLOT( _eventSerialPortTimeout() )
            );
}

void SerialRelay::activateRelay()
{
    if (_isNotConfigured) { return; }
    activatePersistentRelay();
    QTimer::singleShot(_activationTimeMs,
                       this, SLOT( _eventDispatchTimerTimeout() )
                       );
}

void SerialRelay::activatePersistentRelay()
{
    if (_isNotConfigured) { return; }

    if (isNull()) {
        LOG_ERROR("Attempting to activate a null relay object.");
        return;
    }
    QByteArray cmd = _createActivateCommand();
    _write(cmd);
}

void SerialRelay::deactivateRelay()
{
    if (_isNotConfigured) { return; }

    if (isNull()) {
        LOG_ERROR("Attempting to activate a null relay object.");
        return;
    }
    QByteArray cmd = _createDeactivateCommand();
    _write(cmd);
}

bool SerialRelay::isNull()
{
    return !_isInit;
}

QString SerialRelay::getPortInfoStr()
{
    QString info;
    info += "Name: " + _portInfo.portName() + "\n";
    info += "Location: " + _portInfo.systemLocation() + "\n";
    info += "Description: " + _portInfo.description() + "\n";
    info += "Mfg: " + _portInfo.manufacturer() + "\n";
    info += "Serial: " + _portInfo.serialNumber() + "\n";
    info += QString("Vendor ID: ") +
            QString::number(_portInfo.vendorIdentifier()) + "\n";
    info += QString("Product ID: ") +
            QString::number(_portInfo.productIdentifier()) + "\n";
    info += QString("Busy: ") +
            (_portInfo.isBusy() ? "true" : "false") + "\n";
    info += QString("Valid: ") +
            (!_portInfo.isNull() ? "true" : "false") + "\n";
    return info;
}

QString SerialRelay::getShortPortInfoStr()
{
    return _portInfo.portName() + " (" + _portInfo.description() + ") " +
            "Valid: " + (_portInfo.isBusy() ? "true" : "false") +
            " Busy: " + (_portInfo.isBusy() ? "true" : "false");
}

QSerialPortInfo SerialRelay::getPortInfoObj()
{
    return _portInfo;
}

QByteArray SerialRelay::_createActivateCommand()
{
    QString cmd = "relay on " + QString::number(_switchNumber);
    cmd.append(EXECUTE_CMD);
    return cmd.toLocal8Bit();
}

QByteArray SerialRelay::_createDeactivateCommand()
{
    QString cmd = "relay off " + QString::number(_switchNumber);
    cmd.append(EXECUTE_CMD);
    return cmd.toLocal8Bit();
}

void SerialRelay::_write(QByteArray cmd)
{
    if (cmd.isEmpty()) {
        LOG_ERROR(getName() + " Attempted to send empty command");
    }

    bool isValidCmd = (cmd.right(1).at(0) == EXECUTE_CMD);
    if (!isValidCmd) {
        LOG_ERROR(getName() + "Error: command not ending in return character: "
                 + cmd + " (" + _hexdump(cmd) + ")");
        cmd += EXECUTE_CMD;
    }

    if (_serialPort.isOpen()) {
        LOG_WARN(getName() + " Trying to write to open port. Closing.");
        _serialPort.close();
    }
    _writeData = cmd;

    if (_portInfo.isBusy()) {
        if (_busyRetries > MAX_BUSY_RETRIES) {
            LOG_WARN(getName() + " port busy timeout.");
            if (_serialPort.isOpen()) { _serialPort.close(); }
            _busyRetries = 0;
            return;
        }
        QTimer::singleShot(BUSY_WAIT_MS, this, SLOT(_eventRetryWrite()));
        ++_busyRetries;
        return;
    }
    _busyRetries = 0;

    LOG_INFO(getName() + " Writing to serial port: <<" + cmd + ">> ("
             + _hexdump(cmd) + ")");

    bool isOpen = _serialPort.open(QIODevice::WriteOnly);

    if (!isOpen) {
        LOG_WARN(getName() + " Problm opening port: "
                 + _serialPort.errorString());
        return;
    }

    _serialPort.clear();

    const qint64 bytesWritten = _serialPort.write(cmd);

    bool isWriteError = bytesWritten == -1;
    bool isCutOff = bytesWritten != _writeData.size();

    if (isWriteError) {
        LOG_WARN(getName() + " write error: " + _serialPort.errorString());
    }

    if (isCutOff) {
        LOG_WARN(getName() + " write cut off problem: "
                 + _serialPort.errorString());
    }

    _serialPort.flush();
    _writeTimer.start();
}

QString SerialRelay::_hexdump(QByteArray bytes)
{
    QString str = "|";
    for (int i = 0; i < bytes.size(); ++i) {
        str += QString::number((int)bytes.at(i));
        str += "|";
    }
    return str;
}

void SerialRelay::_eventBytesWritten(qint64 bytes)
{
    _bytesWritten += bytes;
    bool isDoneWriting = _bytesWritten == _writeData.size();
    if ( isDoneWriting ) {
        LOG_INFO(getName() + " total bytes written: "
                 + QString::number(_bytesWritten));
        _bytesWritten = 0;
        if (_serialPort.isOpen()) { _serialPort.close(); }
        _writeTimer.stop();
    }
}

void SerialRelay::_eventSerialPortTimeout()
{
    LOG_WARN(getName() + " write timeout: " + _serialPort.errorString()
             + " busy: " + (_portInfo.isBusy() ? "true" : "false")
             + " open: " + (_serialPort.isOpen() ? "true" : "false"));
    if (_serialPort.isOpen()) { _serialPort.close(); }
}

void SerialRelay::_eventWriteError(QSerialPort::SerialPortError error)
{
    bool isWriteError = error == QSerialPort::WriteError;
    bool isNoError = error == QSerialPort::NoError;
    if (isWriteError) {
        LOG_WARN(getName() + " I/O error while writing: "
                 + _serialPort.errorString());
    }
    else if (!isNoError) {
        LOG_WARN(getName() + " misc error while writing: "
                 + _serialPort.errorString() + error);
    }

    if (_serialPort.isOpen()) { _serialPort.close(); }
    _writeTimer.stop();

}

void SerialRelay::_eventRetryWrite()
{
    _write(_writeData);
}

void SerialRelay::_eventDispatchTimerTimeout()
{
    deactivateRelay();
}

void SerialRelay::_eventDataTerminalReadyChanged(bool set)
{
    LOG_INFO(getName() + " data terminal ready state changed: " + set);
}

} // stnctl
