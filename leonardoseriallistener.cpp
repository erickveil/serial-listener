// Copyright 2021 Erick Veil

#include "leonardoseriallistener.h"

LeonardoSerialListener::LeonardoSerialListener(QObject *parent) : QObject(parent)
{

}

void LeonardoSerialListener::init()
{
    if (_isInit) { return; }
    _isInit = true;

    QSerialPort::BaudRate baud = QSerialPort::Baud115200;
    QString port = _getArduinoPort();

    _connection.setPortName(port);
    _connection.setBaudRate(baud);

    connect(&_connection, SIGNAL(aboutToClose()),
            this, SLOT(_eventAboutToClose()));
    connect(&_connection, SIGNAL(bytesWritten(qint64)),
            this, SLOT(_eventBytesWritten(qint64)));
    connect(&_connection, SIGNAL(channelBytesWritten(int,qint64)),
            this, SLOT(_eventChannelBytesWritten(int,qint64)) );
    connect(&_connection, SIGNAL(channelReadyRead(int)),
            this, SLOT(_eventChannelReadyRead(int)) );
    connect(&_connection, SIGNAL(readChannelFinished()),
            this, SLOT(_eventReadChannelFinished()) );
    connect(&_connection, SIGNAL(readyRead()),
            this, SLOT(_eventReadyRead()) );

}

void LeonardoSerialListener::init(std::function<void (QByteArray)> parseCb)
{
    _parseCallback = parseCb;
    init();
}

void LeonardoSerialListener::start()
{
    QString msg;
    if (!_isInit) {
        msg = "Attempting to start a serial port listener without "
              "initnialization.";
        LOG_ERROR(msg);
        return;
    }

    bool isOpenSuccess = _connection.open(QIODevice::ReadOnly);
    if (!isOpenSuccess) {
        msg = getName() +
                ": Failure opening serial port listener: " +
                _connection.errorString();
        LOG_WARN(msg);
        return;
    }
    LOG_INFO(getName() + ": Open for reading");
}

void LeonardoSerialListener::logSerialInfo()
{
    QString logMsg;
    QList<QSerialPortInfo> serialList = QSerialPortInfo::availablePorts();
    logMsg = "There are " + QString::number(serialList.count()) +
            " available serial ports:\n";
    for (int i = 0; i < serialList.count(); ++i) {
        QSerialPortInfo info = serialList[i];
        logMsg += "Port " + QString::number(i) + ":\n" +
                "Description: " + info.description() + "\n" +
                "Manufacturer: " + info.manufacturer() + "\n" +
                "Port Name: " + info.portName() + "\n" +
                "Product ID: " + QString::number(info.productIdentifier()) + "\n" +
                "Serial Number: " + info.serialNumber() + "\n" +
                "System Location: " + info.systemLocation() + "\n" +
                "Vendor ID: " + QString::number(info.vendorIdentifier()) + "\n\n";
    }

    LOG_INFO(logMsg);
}

QString LeonardoSerialListener::getName()
{
    return _serialInfo.description() + "@" + _serialInfo.portName();
}

void LeonardoSerialListener::_eventAboutToClose()
{
    LOG_DEBUG(getName() + ": serial about to close");

}

void LeonardoSerialListener::_eventBytesWritten(qint64 bytes)
{
    LOG_DEBUG(getName() + ": serial bytes written: " + QString::number(bytes));

}

void LeonardoSerialListener::_eventChannelBytesWritten(int channel, qint64 bytes)
{
    LOG_DEBUG(getName() + ": serial channel bytes written. Channel: " +
             QString::number(channel) + " Bytes: " + QString::number(bytes));

}

void LeonardoSerialListener::_eventChannelReadyRead(int channel)
{
    LOG_DEBUG(getName() + ": serial channel ready read: " + QString::number(channel));

}

void LeonardoSerialListener::_eventReadChannelFinished()
{
    LOG_INFO(getName() + ": serial read channel finished");

}

void LeonardoSerialListener::_eventReadyRead()
{
    LOG_INFO(getName() + ": Receiving data...");
    QByteArray readBuffer = _connection.readAll();
    LOG_INFO(getName() + ": Data recieved: " + readBuffer);
    _connection.flush();

    if (_parseCallback) { _parseCallback(readBuffer); }
}

QString LeonardoSerialListener::_getArduinoPort()
{
    QList<QSerialPortInfo> serialList = QSerialPortInfo::availablePorts();

    if (serialList.count() == 0) {
        QString msg = "Could not find any serial ports for listener!";
        LOG_ERROR(msg);
        return "";
    }

    for (int i = 0; i < serialList.count(); ++i) {
        QSerialPortInfo info = serialList[i];
        QString description = info.description();
        if (!description.contains("Arduino")) { continue; }
        QString portLocation = info.systemLocation();
        _serialInfo = info;
        QString msg = getName() + ": Found Arduino port at " + portLocation +
                " setting up listener there.";
        LOG_INFO(msg);
        return portLocation;
    }

    QString defaultPort = serialList[0].systemLocation();
    _serialInfo = serialList[0];
    QString msg = "Could not find the Arduino port automatically. "
                  "Defaulting listener to " + getName() + " at " + defaultPort;
    return defaultPort;
}
