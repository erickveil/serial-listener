#include <QCoreApplication>

#include "functional"

#include "leonardoseriallistener.h"
#include "staticlogger.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    LeonardoSerialListener::logSerialInfo();

    auto parseCb = [&] (QByteArray msg) {
        LOG_INFO("Parsing message: " + msg);
    };


    LeonardoSerialListener serialListener;
    serialListener.init(parseCb);
    serialListener.start();

    return a.exec();
}
